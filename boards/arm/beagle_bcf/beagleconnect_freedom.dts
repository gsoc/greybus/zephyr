/*
 * Copyright (c) 2020 Erik Larson
 * Copyright (c) 2020 Jason Kridner, BeagleBoard.org Foundation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/dts-v1/;

#include <ti/cc1352r.dtsi>
#include <zephyr/dt-bindings/pinctrl/cc13xx_cc26xx-pinctrl.h>

#define BTN_GPIO_FLAGS (GPIO_ACTIVE_LOW | GPIO_PULL_UP)

/ {
    model = "BeagleConnect Freedom";
    compatible = "beagle,beagleconnect-freedom";

    aliases {
		led0 = &led0;
		sw0 = &button0;
        sensor0 = &light;
        sensor1 = &accel;
        sensor2 = &humidity;  /* Not populated on rev C4 */
    };

    chosen {
        zephyr,sram = &sram0;
        zephyr,console = &uart0;
        zephyr,shell-uart = &uart0;
        zephyr,flash = &flash0;
    };

    gpio_keys {
        compatible = "gpio-keys";
        button0: button_0 {
            label = "User button";
            gpios = <&gpio0 15 GPIO_ACTIVE_LOW>;
        };
    };

	leds: leds {
		compatible = "gpio-leds";
		led0: led_0 {
			gpios = <&gpio0 18 GPIO_ACTIVE_HIGH>; // 2.4GHz TX/RX
			label = "LED";
		};

        /* U.FL connector switch */
        rf_sw: rf_sw {
            label = "RF_SW";
            gpios =
                <&gpio0 29 GPIO_ACTIVE_HIGH>, // SubG TX +20dB
                <&gpio0 30 GPIO_ACTIVE_HIGH>; // SubG TX/RX 0dB
        };
	};

    sens_i2c: sensor-switch {
        compatible = "ti,ts5a2066";
        #address-cells = <1>;
        #size-cells = <0>;
        controller = <&i2c0>;
        gpios = <&gpio0 14 GPIO_ACTIVE_HIGH>;
        label = "I2C_0S";

        accel: lis2dh12-accel@18 {
            compatible = "st,lis2dh12", "st,lis2dh", "st,lis2de12";
            reg = <0x18>;
            label = "LIS2DE12-ACCEL";
        };

        light: opt3001-light@44 {
            compatible = "ti,opt3001";
            reg = <0x44>;
            label = "OPT3001-LIGHT";
        };

        humidity: hdc2010-humidity@41 {
		status = "disabled";
            compatible = "ti,hdc2010";
            reg = <0x41>;
            label = "HDC2010-HUMIDITY";
        };
    };

	power-states {
		idle: idle {
			compatible = "zephyr,power-state";
			power-state-name = "suspend-to-idle";
			min-residency-us = <1000>;
		};

		standby: standby {
			compatible = "zephyr,power-state";
			power-state-name = "standby";
			min-residency-us = <5000>;
			exit-latency-us = <240>;
		};
	};
};

&pinctrl {
	/* UART0 */
	uart0_tx_default: uart0_tx_default {
		pinmux = <13 IOC_PORT_MCU_UART0_TX>;
		bias-disable;
	};
	uart0_rx_default: uart0_rx_default {
		pinmux = <12 IOC_PORT_MCU_UART0_RX>;
		bias-disable;
		input-enable;
	};

	/* UART1 */
	uart1_tx_default: uart1_tx_default {
		pinmux = <22 IOC_PORT_MCU_UART1_TX>;
		bias-disable;
	};
	uart1_rx_default: uart1_rx_default {
		pinmux = <21 IOC_PORT_MCU_UART1_RX>;
		bias-disable;
		input-enable;
	};

	/* I2C0 */
	i2c0_scl_default: i2c0_scl_default {
		pinmux = <25 (IOC_PORT_MCU_I2C_MSSCL | IOC_CURRENT_8MA | IOC_STRENGTH_MAX)>;
		bias-pull-up;
		drive-open-drain;
		input-enable;
	};
	i2c0_sda_default: i2c0_sda_default {
		pinmux = <26 (IOC_PORT_MCU_I2C_MSSDA | IOC_CURRENT_8MA | IOC_STRENGTH_MAX)>;
		bias-pull-up;
		drive-open-drain;
		input-enable;
	};
	i2c0_scl_sleep: i2c0_scl_sleep {
		pinmux = <25 IOC_PORT_GPIO>;
		bias-disable;
	};
	i2c0_sda_sleep: i2c0_sda_sleep {
		pinmux = <26 IOC_PORT_GPIO>;
		bias-disable;
	};

	/* SPI0 */
	spi0_sck_default: spi0_sck_default {
		pinmux = <10 IOC_PORT_MCU_SSI0_CLK>;
		bias-disable;
	};
	spi0_mosi_default: spi0_mosi_default {
		pinmux = <9 IOC_PORT_MCU_SSI0_TX>;
		bias-disable;
	};
	spi0_miso_default: spi0_miso_default {
		pinmux = <11 IOC_PORT_MCU_SSI0_RX>;
		bias-disable;
		input-enable;
	};
	spi0_cs0_default: spi0_cs0_default {
		pinmux = <8 IOC_PORT_GPIO>;
		bias-disable;
	};
	spi0_cs1_default: spi0_cs1_default {
		pinmux = <28 IOC_PORT_GPIO>;
		bias-disable;
	};
	spi0_cs2_default: spi0_cs2_default {
		pinmux = <27 IOC_PORT_GPIO>;
		bias-disable;
	};
};

&cpu0 {
	clock-frequency = <48000000>;
	cpu-power-states = <&idle &standby>;
};

&trng {
    status = "okay";
};

&gpio0 {
    status = "okay";
};

/* On C5, side away from battery connector (with MSP430) */
/* Wrong pinout on header */
&uart0 {
    status = "okay";
    current-speed = <115200>;
	pinctrl-0 = <&uart0_rx_default &uart0_tx_default>;
	pinctrl-names = "default";
};

/* On C5, side with battery connector (with CC1352 and not MSP430) */
/* Correct pinout on header */
&uart1 {
    status = "okay";
    current-speed = <115200>;
	pinctrl-0 = <&uart1_rx_default &uart1_tx_default>;
	pinctrl-names = "default";
};

&i2c0 {
    status = "okay";
    clock-frequency = <I2C_BITRATE_FAST>;
	pinctrl-0 = <&i2c0_scl_default &i2c0_sda_default>;
	pinctrl-1 = <&i2c0_scl_sleep &i2c0_sda_sleep>;
	pinctrl-names = "default", "sleep";

    mcu: msp430-usbbridge@4 {
        compatible = "beagle,usbbridge";
        reg = <0x4>;
        label = "MSP430-USBBRIDGE";
    };
};

&spi0 {
    status = "okay";
	pinctrl-0 = <&spi0_sck_default &spi0_mosi_default
		&spi0_miso_default &spi0_cs0_default
		&spi0_cs1_default &spi0_cs2_default>;
	pinctrl-names = "default";
    cs-gpios = <&gpio0 8 GPIO_ACTIVE_LOW>,
        <&gpio0 28 GPIO_ACTIVE_LOW>,  // mikroBUS port 1
        <&gpio0 27 GPIO_ACTIVE_LOW>;  // mikroBUS port 2

    nor_flash: gd25q16c@0 {
        compatible = "jedec,spi-nor";
        label = "GD25Q16C";
        reg = <0>;
        spi-max-frequency = <2000000>;
        size = <0x200000>;
        //has-be32k;
        has-dpd;
        t-enter-dpd = <20000>;
        t-exit-dpd = <100000>;
        jedec-id = [c8 40 15];
    };
};

&rtc {
    status = "okay";
};
